#!/bin/bash

localPrim=$(find / -type f -name $1 2> /dev/null)
localSegun=$(find / -type f -name $2 2> /dev/null)
localTerc=$(find / -type f -name $3 2> /dev/null)
localQuart=$(find / -type f -name $4 2> /dev/null)
primeiro=$(wc -l <$localPrim)
segundo=$(wc -l <$localSegun)
terceiro=$(wc -l <$localTerc)
quarto=$(wc -l <$localQuart)
[ $primeiro -gt $segundo -a $primeiro -gt $terceiro -a $primeiro -gt $quarto ] && echo $primeiro
[ $segundo -gt $primeiro -a $segundo -gt $terceiro -a $segundo -gt $quarto ] && echo $segundo
[ $terceiro -gt $primeiro -a $terceiro -gt $segundo -a $terceiro -gt $quarto ] && echo $terceiro
[ $quarto -gt $primeiro -a $quarto -gt $segundo -a $quarto -gt $terceiro ] && echo $quarto
